<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\UnitRumah;

class ProductController extends Controller
{
    public function getUnit(){

        $unit = DB::table('unit')->get();

        return view('unit', ["abc" => $unit]);
    }

    public function createUnit(Request $request){
        DB::beginTransaction();

        try{

            $newUnit = new unit;
            $newUnit->kavling = $request->input('kavling');
            $newUnit->block = $request->input('block');
            $newUnit->no_rumah = $request->input('no_rumah');
            $newUnit->harga_rumah = $request->input('harga_rumah');
            $newUnit->luas_tanah = $request->input('luas_tanah');
            $newUnit->luas_bangunan = $request->input('luas_bangunan');
            $newUnit->save();

            DB::commit();
            return response()->json(["message"=>"Success"], 200);

        }catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=> $e->getMessage ], 500);
        }
        
    }

    function deleteUnit($id){
        DB::delete('delete from unit where id = ?', [$id]);
        return redirect("/");
    }
}
